https://github.com/transparency-dev/armored-witness/blob/main/docs/custodian.md

```
leguin:armored-witness% GOBIN=${PWD} go install github.com/transparency-dev/armored-witness/cmd/verify@main
go: downloading github.com/transparency-dev/armored-witness v0.0.0-20240524150942-06e98d6458c1
go: github.com/transparency-dev/armored-witness/cmd/verify@main: github.com/transparency-dev/armored-witness@v0.0.0-20240524150942-06e98d6458c1 requires go >= 1.22.0 (running go 1.21.10; GOTOOLCHAIN=local)
```

```
podman pull docker.io/library/debian
podman run --rm -it --volume /home/linus/usr/src/glasklar/services/armored-witness:/c:z docker.io/library/debian bash -
apt update; apt upgrade -y; apt install -y ca-certificates
echo deb https://deb.debian.org/debian unstable main > /etc/apt/sources.list.d/sid.list
apt update
apt install -y -t unstable golang
cd /c; mkdir -p bin
GOBIN=${PWD}/bin go install github.com/transparency-dev/armored-witness/cmd/verify@main
exit
```

See data/2024-07-01T18:44-armored-witness-usb-attach.txt for USB details.

See data/2024-07-01T19:37-armored-witness-verify.txt for verification.
