Fri Nov 29 10:42:06 CET 2024
----------------------------------------------------------- Trusted OS ----
Serial number ..............: 720A9DEAD4391737
Secure Boot ................: true
SRK hash ...................: 77e021cc51b5547fb0c2192fb32710bfa89b4bbaa7dab5f97fc585f673b0b236
Revision ...................: 3e1e861
Version ....................: 0.3.0
Runtime ....................: go1.23.1 tamago/arm
Link .......................: true
MAC ........................: de:62:05:b8:85:e9
IdentityCounter ............: 0
Witness/Identity ...........: ArmoredWitness-small-breeze+7b3ada3a+AQAWYpUjfGGPQiWOgt6sLWap0P9rVOe+Ofu2uNZbO4FD
Witness/IP .................: **REDACTED**
Witness/AttestationKey .....: AW-ID-Attestation-720A9DEAD4391737+2f008071+ARa8g49Anj+op8IBReuNLXJtkjGoB2WzOb5ytUaOEheH
Witness/AttestedIdentity ...: [below]

ArmoredWitness ID attestation v1
720A9DEAD4391737
0
ArmoredWitness-small-breeze+7b3ada3a+AQAWYpUjfGGPQiWOgt6sLWap0P9rVOe+Ofu2uNZbO4FD

— AW-ID-Attestation-720A9DEAD4391737 LwCAcSPENAsE+KSQpeGr9Z/N3vWgeE5h7LWQLy0BDWQJj2LHZOin8Tz79a3fymhHFXPjXoDfJxZrmbbs8xARmUGm1w4=

Witness/AttestedBastionID ..: [below]

ArmoredWitness BastionID attestation v1
720A9DEAD4391737
0
2867895f07dfc47299cf7d2ced88ed5230a822a7f62d54f8402d6daf11520131

— AW-ID-Attestation-720A9DEAD4391737 LwCAcXIiJinSPvqCOqODTY6L1w9krYI0MHgzkt2ccTbeYZ1bFUVj8Rg5ZDNaKAv3DqzcC64jrB3pqswNQAV/PsSs0wM=

----------------------------------------------------------- Trusted OS ----
