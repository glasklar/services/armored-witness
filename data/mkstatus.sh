#! /bin/sh
set -eu
date
curl -s http://"$1":8081/status | sed -e "s/$1/**REDACTED**/g"
echo

